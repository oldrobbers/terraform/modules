# Create bucket
resource "yandex_storage_bucket" "bucket" {
  for_each   = var.buckets

  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket     = each.key
  acl        = each.value.acl
}
