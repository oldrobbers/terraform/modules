variable "token" {
  type = string
}

variable "cloud_id" {
  type = string
}

variable "folder_id" {
  type = string
}

variable "zone" {
  type = string
}

variable "service_account_name" {
  type = string
}

variable "buckets" {
  type = map(object({
    acl = string
  }))
}
